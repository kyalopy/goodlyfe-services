from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
                       url(r'^$', 'GoodLyf.views.home', name='home'),
                       url(r'^chaining/', include('smart_selects.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^goodlyf/', include('goodlyf.urls')),

                       )
if settings.DEBUG:
    urlpatterns += patterns(
        'django.views.static',
        (r'^media/(?P<path>.*)',
        'serve',
        {'document_root': settings.MEDIA_ROOT}), )

admin.site.site_header = settings.ADMIN_SITE_HEADER
