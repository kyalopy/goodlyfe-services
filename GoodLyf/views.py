__author__ = 'steven'
from django.shortcuts import render
from goodlyf.models import ServiceCategory, ServiceMenu

def home(request):
    service_categories = ServiceCategory.objects.all()[:4]
    service_categories_next = ServiceCategory.objects.all()[5:9]
    service_categories_last = ServiceCategory.objects.all()[10:11]

    cleaning_services = ServiceMenu.objects.filter(category_id=1)
    gardening_services = ServiceMenu.objects.filter(category_id=9)
    handyman_services = ServiceMenu.objects.filter(category_id=11)
    installation_services = ServiceMenu.objects.filter(category_id=3)
    painting_services = ServiceMenu.objects.filter(category_id=10)
    petcare_services = ServiceMenu.objects.filter(category_id=4)
    plumbing_services = ServiceMenu.objects.filter(category_id=6)
    transport_services = ServiceMenu.objects.filter(category_id=5)

    return render(request, 'index.html', {'service_categories': service_categories,
                                          'service_categories_next': service_categories_next,
                                          'service_categories_last': service_categories_last,
                                          'cleaning_services': cleaning_services,
                                          'gardening_services': gardening_services,
                                          'handyman_services': handyman_services,
                                          'installation_services': installation_services,
                                          'painting_services': painting_services,
                                          'petcare_services': petcare_services,
                                          'plumbing_services': plumbing_services,
                                          'transport_services': transport_services,
                                          })
