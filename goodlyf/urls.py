__author__ = 'root'
from django.conf.urls import url, patterns
from goodlyf.views import *

urlpatterns = patterns('',
                       url(r'^register/$', register, name='register'),
                       url(r'^login/$', user_login, name='login'),
                       url(r'^logout/$', user_logout, name='logout'),
                       url(r'^create_staff', create_staff, name='create_staff'),
                       url(r'^create_handyman', create_handyman, name='create_handyman'),
                       url(r'^booking', booking, name='booking'),
                       url(r'^incoming-jobs/$', incoming_jobs, name='incoming_jobs'),
                       url(r'^cancelled-jobs/$', cancelled_order, name='cancelled_jobs'),
                       url(r'^(?P<job_id>\d+)/$', job, name='job'),
                       url(r'^assign/(?P<order_id>\d+)/$', assign, name='assign'),
                       url(r'^worker-details/(?P<worker_id>\d+)/$', worker_details, name='worker_details'),
                       url(r'^my-jobs/$', my_jobs, name='my_jobs'),
                       url(r'^dashboard/$', dashboard, name='dashboard'),
                       url(r'^thank-you/$', thank_you, name='thank_you'),
                       url(r'^workers-profile/$', worker_profile, name='worker_profile'),
                       url(r'^complete-jobs/$', complete_jobs, name='complete_jobs'),
                       url(r'^edit/(?P<job_id>\d+)/$', edit_job, name='edit_job'),
                       url(r'^push-to-admin/(?P<job_id>\d+)/$', cancelled_job, name='cancelled_job'),
                       url(r'^client/(?P<order_id>\d+)/$', client, name='client'),
                       url(r'^delete/(?P<job_id>\d+)/$', complete_job_delete, name='complete_job_delete'),
                       url(r'^search/$', search_worker, name='search'),
                       )
