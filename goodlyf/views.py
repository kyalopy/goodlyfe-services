from django.shortcuts import render, HttpResponseRedirect, get_object_or_404, redirect, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from goodlyf.forms import *
from django.contrib import messages
from django.core.urlresolvers import reverse
import datetime
from django.db.models import Count
from goodlyf.models import Service, ServiceMenu


@user_passes_test(lambda u: u.is_superuser)
def register(request):

    if request.method == 'POST':
        user_form = UserForm(request.POST)
        worker_form = WorkerForm(request.POST, request.FILES)
        if User.objects.filter(email=request.POST['email']).exists():
            email_error = "This email is already in use"
            return render(request, 'goodlyf/register.html', {'user_form': user_form, 'worker_form': worker_form,
                                                             'email_error': email_error})
        password1 = request.POST['password']
        password2 = request.POST['password_confirmation']
        if password1 != password2:
            password_error = "Password didn't match"
            return render(request, 'goodlyf/register.html', {'user_form': user_form, 'worker_form': worker_form,
                                                            'password_error': password_error})

        if user_form.is_valid() and worker_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()

            worker = worker_form.save(commit=False)
            worker.user = user
            worker.save()

            user_form = UserForm()
            worker_form = WorkerForm()

            return render(request, 'goodlyf/register.html', {'user_form': user_form,
                                                             'worker_form': worker_form, })

        else:
            user_form = UserForm(request.POST)
            worker_form = WorkerForm(request.POST, request.FILES)
            return render(request, 'goodlyf/register.html', {'user_form': user_form, 'worker_form': worker_form})

    else:
        user_form = UserForm()
        worker_form = WorkerForm()

        return render(request, 'goodlyf/register.html', {'user_form': user_form, 'worker_form': worker_form})


def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)

                return HttpResponseRedirect('/')
            else:
                is_active_error = "The  account you are trying to use is not active. Please conduct " \
                                  "your system admin"
                return render(request, 'goodlyf/login.html', {'is_active_error': is_active_error})
        else:
            invalid_details_error = "You entered invalid login details. Please try again"
            return render(request, 'goodlyf/login.html', {'invalid_details_error': invalid_details_error})
    else:
        return render(request, 'goodlyf/login.html',)


@login_required
def user_logout(request):
    logout(request)

    return HttpResponseRedirect('/')


@user_passes_test(lambda u: u.is_superuser)
def create_staff(request):
    workers_count = (Worker.objects.all().filter(worker_category__category_name='Staff').count() +
    Worker.objects.all().filter(worker_category__category_name='Supervisor').count())
    if request.method == 'POST':
        # forms
        user_form = UserForm(request.POST)
        worker_form = WorkerForm(request.POST, request.FILES)

        # form validations pragmatically
        if User.objects.filter(email=request.POST['email']).exists():
            email_error = 'The email is already in use'
            return render(request, 'goodlyf/create_staff.html', {'user_form': user_form, 'worker_form': worker_form,
                                                                 'email_error': email_error, })
        password1 = request.POST['password']
        password2 = request.POST['password_confirmation']
        if password1 != password2:
            password_error = "Password didn't match"
            return render(request, 'goodlyf/create_staff.html', {'user_form': user_form, 'worker_form': worker_form,
                                                                 'password_error': password_error, })

        if request.user.is_superuser:
            request.POST['worker_category'] = 2

        if user_form.is_valid() and worker_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()

            worker = worker_form.save(commit=False)
            worker.user = user
            worker.save()
            messages.info(request, "New staff/supervisor added ")
            user_form = UserForm()
            worker_form = WorkerForm()
            workers_count = (Worker.objects.all().filter(worker_category__category_name='Staff').count() +
    Worker.objects.all().filter(worker_category__category_name='Supervisor').count())

            return render(request, 'goodlyf/create_staff.html', {'user_form': user_form,
                                                                 'worker_form': worker_form,
                                                                 'workers_count': workers_count
                                                                 })
        else:
            user_form = UserForm(request.POST)
            worker_form = WorkerForm(request.POST, request.FILES)
            return render(request, 'goodlyf/create_staff.html', {'user_form': user_form,
                                                                 'worker_form': worker_form,
                                                                 'workers_count': workers_count})
    else:
        user_form = UserForm()
        worker_form = WorkerForm()

        return render(request, 'goodlyf/create_staff.html', {'user_form': user_form, 'worker_form': worker_form,
                                                             'workers_count': workers_count})


@user_passes_test(lambda u: u.is_superuser)
def create_handyman(request):
    workers_count = Worker.objects.all().count()
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        worker_form = WorkerForm(request.POST, request.FILES)

        if User.objects.filter(email=request.POST['email']).exists():
            email_error = 'The email is already in use'
        if User.objects.filter(username=request.POST['username']).exists():
            username_error = 'The username is already in use'
            return render(request, 'goodlyf/create_handyman.html',
                          {'user_form': user_form, 'worker_form': worker_form,
                           'email_error': email_error,
                           'username_error': username_error
                           })
        password1 = request.POST['password']
        password2 = request.POST['password_confirmation']
        if password1 != password2:
            password_error = "Password didn't match"
            return render(request, 'goodlyf/create_handyman.html', {'user_form': user_form, 'worker_form': worker_form,
                                                                    'password_error': password_error,

                                                                    })

        if request.user.is_superuser or request.user.is_staff:
            request.POST['worker_category'] = 3
        else:
            worker_error = "Please choose the correct category"
            return render(request, 'goodlyf/create_handyman.html', {'user_form': user_form, 'worker_form': worker_form,
                                                                    'worker_error': worker_error,
                                                                    'workers_count': workers_count,
                                                                    })

        if user_form.is_valid() and worker_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()

            worker = worker_form.save(commit=False)
            worker.user = user
            worker.save()
            messages.info(request, "New handyman added")
            user_form = UserForm()
            worker_form = WorkerForm()
            workers_count = Worker.objects.all().count()

            return render(request, 'goodlyf/create_handyman.html', {'user_form': user_form, 'worker_form': worker_form,
                                                                    'workers_count': workers_count})
        else:
            user_form = UserForm(request.POST)
            worker_form = WorkerForm(request.POST, request.FILES)

            return render(request, 'goodlyf/create_handyman.html', {'user_form': user_form,
                                                                    'worker_form': worker_form,
                                                                    })
    else:
        user_form = UserForm()
        worker_form = WorkerForm()

        return render(request, 'goodlyf/create_handyman.html',
                      {'user_form': user_form, 'worker_form': worker_form, 'workers_count': workers_count
                       },)


def booking(request):
    if request.method == 'POST':
        job_form = IncomingJobForm(request.POST)
        today = datetime.date.today()
        pick_date = request.POST['date_job_starts']
        print(today, pick_date)

        if str(pick_date) < str(today):
            past_date_error = "Sorry, the date you selected is passed"
            messages.info(request, past_date_error)
            return render(request, 'goodlyf/booking.html', {'job_form': job_form})

        if job_form.is_valid():

            order = job_form.save()
            return HttpResponseRedirect(reverse('client', args=(order.id, )))

        else:
            job_form = IncomingJobForm(request.POST)
            return render(request, 'goodlyf/booking.html', {'job_form': job_form, }, )
    else:

        job_form = IncomingJobForm()
        return render(request, 'goodlyf/booking.html', {'job_form': job_form, }, )


def client(request, order_id):
    order = get_object_or_404(IncomingJob, pk=order_id)
    try:
        service_budget = ServiceMenu.objects.filter(name_id=order.service_name_id).first()
    except ServiceMenu.DoesNotExist:
        service_budget = None

    if request.method == 'POST':
        booking_client_form = ClientForm(request.POST)
        request.POST['job'] = order.id

        if booking_client_form.is_valid():
            client_info = booking_client_form.save(commit=False)
            client_info.job_id = order.id
            client_info.save()

            return HttpResponseRedirect(reverse('thank_you'))
        else:
            return render(request, 'goodlyf/booking-client.html', {'booking_client_form': booking_client_form,
                                                                   'order': order,
                                                                   'service_budget': service_budget})
    else:
        booking_client_form = ClientForm()
        return render(request, 'goodlyf/booking-client.html', {'booking_client_form': booking_client_form,
                                                               'order': order,
                                                               'service_budget': service_budget})


def thank_you(request):
    return render(request, 'goodlyf/thank-you.html')


@user_passes_test(lambda u: u.is_superuser)
def incoming_jobs(request):
    jobs_by_date = IncomingJob.objects.all()

    return render(request, 'goodlyf/incoming-jobs.html', {'jobs_by_date': jobs_by_date})


@user_passes_test(lambda u: u.is_staff)
def job(request, job_id):
    job = get_object_or_404(IncomingJob, pk=job_id)
    client = Client.objects.filter(job=job_id).first()

    if job:

        job.state = 1
        job.save()

    return render(request, 'goodlyf/job-details.html', {'job': job, 'client': client })


@user_passes_test(lambda u: u.is_superuser)
def assign(request, order_id):
    supervisor_job = JobAssignment.objects.values('assigned_to').annotate(the_count=Count('assigned_to'))
    supervisor = Worker.objects.all().filter(worker_category__category_name='Supervisor')

    order = get_object_or_404(IncomingJob, pk=order_id)

    if request.method == 'POST':
        """get job"""
        job_assignment_form = JobAssignmentForm(request.POST)
        pick_day = order.date_job_starts
        termination_date = request.POST['date_job_ends']
        request.POST['job'] = order.id
        request.POST['state'] = 1
        request.POST['push_state'] = 0

        if str(termination_date) < str(pick_day):
            past_date_error = "Sorry, the date you selected is passed"
            messages.info(request, past_date_error)
            return HttpResponseRedirect(reverse('assign', args=[order.id]))

        if order.assignment == 1:
            assignment_error = "This job is already assigned a supervisor"
            messages.info(request, assignment_error)
            return HttpResponseRedirect(reverse('assign', args=[order.id]))

        if job_assignment_form.is_valid():
            job_assignment = job_assignment_form.save(commit=False)
            job_assignment.job_id = order.id

            job_assignment_form.save()
            order.assignment = 1
            order.save()
            messages.info(request, "Congratulations! Job assigned successfully! ")
            return HttpResponseRedirect(reverse('assign', args=[order.id]))

        else:
            job_assignment_form = JobAssignmentForm(request.POST)
            print(job_assignment_form.errors)

            return render(request, 'goodlyf/job-assignment.html',
                          {'job_assignment_form': job_assignment_form, 'order': order,
                           'supervisor_job': supervisor_job,'supervisor': supervisor},)

    else:
        job_assignment_form = JobAssignmentForm()
        return render(request, 'goodlyf/job-assignment.html',
                      {'job_assignment_form': job_assignment_form, 'order': order,
                       'supervisor_job': supervisor_job, 'supervisor': supervisor}, )


@user_passes_test(lambda u: u.is_staff)
def my_jobs(request,):
    user_id = request.user.id
    worker_id = Worker.objects.filter(user_id=user_id).first()
    my_job_list = JobAssignment.objects.filter(assigned_to_id=worker_id)
    my_job_count = JobAssignment.objects.filter(assigned_to_id=worker_id).count()
    today = datetime.date.today()

    return render(request, 'goodlyf/my-jobs.html', {'my_job_list': my_job_list,
                                                    'my_job_count': my_job_count,
                                                    'today': today,
                                                    })


def edit_job(request, job_id):
    job = get_object_or_404(JobAssignment, pk=job_id)
    if request.method == 'POST':
        form = JobAssignmentForm(request.POST, instance=job)

        if form.is_valid():
            job.save()

            return HttpResponseRedirect(reverse('my_jobs'))
        else:
            form = JobAssignmentForm(request.POST, instance=job)
            print(form.errors)
            return render(request, 'goodlyf/edit-job-assignment.html', {'form': form})

    else:
        form = JobAssignmentForm(instance=job)
        return render(request, 'goodlyf/edit-job-assignment.html', {'form': form})


@user_passes_test(lambda u: u.is_staff)
def complete_jobs(request):
    jobs = JobAssignment.objects.all().filter(state=2)

    return render(request, 'goodlyf/complete-jobs.html', {'jobs': jobs})


@user_passes_test(lambda u: u.is_staff)
def complete_job_delete(request, job_id):
    job = get_object_or_404(JobAssignment, pk=job_id)

    if request.method == 'POST':
        job.delete()

        return HttpResponseRedirect(reverse('complete_jobs'))
    return render(request, 'goodlyf/delete-complete-job.html', {'job': job})


@user_passes_test(lambda u: u.is_staff)
def cancelled_job(request, job_id):
    job = get_object_or_404(JobAssignment, pk=job_id)

    if request.method == 'POST':
        job_feedback_form = JobFeedbackForm(request.POST)
        request.POST['job'] = job.id

        if job_feedback_form.is_valid:
            job_feedback = job_feedback_form.save(commit=False)
            job_feedback.job_id = job.id
            job.push_state = 1
            job.save()
            job_feedback.save()

            return HttpResponseRedirect(reverse('my_jobs',))
        else:
            print(job_feedback_form.errors)
            return render(request, 'goodlyf/job-feedback.html', {'job_feedback_form': job_feedback_form, 'job': job})
    else:
        job_feedback_form = JobFeedbackForm()
        return render(request, 'goodlyf/job-feedback.html', {'job_feedback_form': job_feedback_form, 'job': job})


@user_passes_test(lambda u: u.is_staff)
def cancelled_order(request):
    orders = JobFeedback.objects.all()

    return render(request, 'goodlyf/cancelled-jobs.html', {'orders': orders})


@user_passes_test(lambda u: u.is_superuser)
def dashboard(request):
    service_count = Service.objects.all().count()
    job_count = IncomingJob.objects.all().count()
    assignment = JobAssignment.objects.all().count()
    complete = JobAssignment.objects.all().filter(state=2).count()
    cancelled = JobAssignment.objects.all().filter(push_state=1).count()
    supervisors = Worker.objects.all().filter(worker_category_id=2)
    supervisors_count = Worker.objects.all().filter(worker_category_id=2).count()
    handymen_count = Worker.objects.all().filter(worker_category_id=3).count()
    staff_count = Worker.objects.all().filter(worker_category_id=1).count()
    supervisor_job = JobAssignment.objects.values('assigned_to').annotate(the_count=Count('assigned_to'))

    return render(request, 'goodlyf/dashboard.html', {'service_count': service_count,
                                                      'job_count': job_count,
                                                      'assignment': assignment,
                                                      'complete': complete,
                                                      'cancelled': cancelled,
                                                      'supervisors': supervisors,
                                                      'supervisors_count': supervisors_count,
                                                      'handymen_count': handymen_count,
                                                      'staff_count': staff_count,
                                                      'supervisor_job': supervisor_job,
                                                      })


@user_passes_test(lambda u: u.is_superuser)
def worker_profile(request):
    workers = Worker.objects.filter()
    cleaning_workers = WorkerSkill.objects.filter(skill_id=1).count()
    pet_workers = WorkerSkill.objects.filter(skill_id=2).count()
    context_dic = {
        'workers': workers,
        'cleaning_workers': cleaning_workers,
        'pet_workers': pet_workers
    }
    return render(request, 'goodlyf/worker-profile.html', context_dic)


def search_worker(request):
    if request.method == 'POST':
        search_text = request.POST['search_text']
    else:
        search_text = ''

    workers = Worker.objects.filter(user__first_name__contains=search_text)
    return render_to_response('worker-search.html', {'workers': workers})


@user_passes_test(lambda u: u.is_superuser)
def worker_details(request, worker_id):
    worker = get_object_or_404(Worker, pk=worker_id)
    worker_residence = WorkerResidence.objects.filter(worker_id=worker.id).first()
    worker_nok = WorkerNextOfKin.objects.filter(worker_id=worker.id).first()
    context_dic = {
        'worker': worker,
        'worker_residence': worker_residence,
        'worker_nok': worker_nok
    }

    return render(request, 'goodlyf/worker-details.html', context_dic)
