from django.contrib import admin
from goodlyf.models import *


class WorkerCategoryAdmin(admin.ModelAdmin):
    list_display = ('category_name', )


class WorkerAdmin(admin.ModelAdmin):
    list_display = ('worker_category', 'phone_number', 'national_id', 'year_of_birth',
                    'profile_image')


class WorkerNextOfKinAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'relationship', 'email', 'phone_number')


class WorkerResidenceAdmin(admin.ModelAdmin):
    list_display = ('worker', 'country', 'county', 'sub_county', 'location', 'village')


class WorkerSkillAdmin(admin.ModelAdmin):
    list_display = ('skill', 'extra_skill', )


class ClientAdmin(admin.ModelAdmin):
    list_display = ('job', 'name', 'phone_number', 'email', 'area', 'street', 'estate', 'house_number')

class ServiceCategoryAdmin(admin.ModelAdmin):
    list_display = ('category_name', )


class ServiceDurationAdmin(admin.ModelAdmin):
    list_display = ('duration', )


class ServicePricingAdmin(admin.ModelAdmin):
    list_display = ('price', )


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', )


class IncomingJobAdmin(admin.ModelAdmin):
    list_display = ('service_category', 'service_name',
                    'date_job_starts', 'state', 'assignment', 'time_remaining' )
    date_hierarchy = 'date_posted'


class JobAssignmentAdmin(admin.ModelAdmin):
    list_display = ('assigned_to', 'date_job_ends', )


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('reviewed_job', 'title', 'text')


class SkillAdmin(admin.ModelAdmin):
    list_display = ('name', )


class NextOfKinRelationAdmin(admin.ModelAdmin):
    list_display = ('name', )


class ServiceMenuAdmin(admin.ModelAdmin):
    list_display = ('category', 'name', 'time', 'price',)


class FeedbackCategoryAdmin(admin.ModelAdmin):
    list_display = ('category_name', )


class SupervisorAssignmentAdmin(admin.ModelAdmin):
    list_display = ('supervisor', 'handyman')

admin.site.register(WorkerCategory, WorkerCategoryAdmin)
admin.site.register(Worker, WorkerAdmin)
admin.site.register(WorkerNextOfKin, WorkerNextOfKinAdmin)
admin.site.register(WorkerResidence, WorkerResidenceAdmin)
admin.site.register(Skill, SkillAdmin)
admin.site.register(WorkerSkill, WorkerSkillAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(ServiceCategory, ServiceCategoryAdmin)
admin.site.register(ServiceDuration, ServiceDurationAdmin)
admin.site.register(ServicePricing, ServicePricingAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(IncomingJob, IncomingJobAdmin)
admin.site.register(JobAssignment, JobAssignmentAdmin)
admin.site.register(SupervisorAssignment, SupervisorAssignmentAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(NextOfKinRelation, NextOfKinRelationAdmin)
admin.site.register(ServiceMenu, ServiceMenuAdmin)
admin.site.register(FeedbackCategory, FeedbackCategoryAdmin)
