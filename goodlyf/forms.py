from goodlyf.models import Worker, Skill, User, WorkerCategory, JobAssignment, WorkerSkill, \
    IncomingJob, WorkerResidence, WorkerNextOfKin, NextOfKinRelation, SupervisorAssignment, \
    JobFeedback, Client
from django import forms
from phonenumber_field.modelfields import PhoneNumberField
from smart_selects.form_fields import ChainedSelect
from goodlyf.choices import *


class UserForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name', max_length=30, min_length=2, widget=forms.TextInput(),
                                 error_messages={'required': 'The first name is required'})
    last_name = forms.CharField(label='Last Name', max_length=30, min_length=2, widget=forms.TextInput(),
                                error_messages={'required': 'The last name is required'})
    username = forms.CharField(label='Username', max_length=30, min_length=3, widget=forms.TextInput())
    email = forms.EmailField(label='Email', widget=forms.EmailInput(),)
    password = forms.CharField(widget=forms.PasswordInput(), label='Type password', min_length=6)
    password_confirmation = forms.CharField(widget=forms.PasswordInput(),  min_length=6,
                                            label='Confirm Password')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password', 'password_confirmation')


class WorkerForm(forms.ModelForm):
    worker_category = forms.ModelChoiceField(queryset=WorkerCategory.objects.all().order_by('category_name'),
                                             show_hidden_initial=False)
    phone_number = PhoneNumberField(help_text="Must start with +254")
    national_id = forms.IntegerField(label='National ID', widget=forms.TextInput(), min_value=10000)
    year_of_birth = forms.DateField(widget=forms.DateInput, label='Year of Birth', required=True,)
    profile_image = forms.ImageField(label='Profile Image',)

    class Meta:
        model = Worker
        fields = ('worker_category', 'phone_number', 'national_id', 'year_of_birth', 'profile_image', )
        exclude = ('user', )


class WorkerSkillForm(forms.ModelForm):
    worker = forms.ModelChoiceField(queryset=Worker.objects.all(), )
    skill = forms.ModelChoiceField(queryset=Skill.objects.all(), label="Skill")
    extra_skill = forms.CharField(widget=forms.TextInput(), label="Other skill(s)")

    class Meta:
        model = WorkerSkill
        fields = ('skill', 'extra_skill', )


class WorkerResidenceForm(forms.ModelForm):
    worker = forms.ModelChoiceField(queryset=Worker.objects.all())
    country = forms.CharField(widget=forms.TextInput(), label="Country", help_text="Kenya",
                              min_length='2', required=True)
    county = forms.CharField(widget=forms.TextInput(), label="County", help_text="Nairobi",
                             min_length='2', required=True)
    sub_county = forms.CharField(widget=forms.TextInput(), label="Sub Country", help_text="Lang'ata",
                                 min_length='2', required=True)
    location = forms.CharField(widget=forms.TextInput(), label="Location", help_text="Barracks",
                               min_length='2', required=True)
    village = forms.CharField(widget=forms.TextInput(), label="Street", help_text="Lorne-Keneth Road",
                              min_length='2', required=True)

    class Meta:
        model = WorkerResidence
        fields = ('county', 'sub_county', 'location', 'village')


class WorkerNextOfKinForm(forms.ModelForm):
    worker = forms.ModelChoiceField(queryset=Worker.objects.all())
    first_name = forms.CharField(widget=forms.TextInput(), label='First Name', min_length='2', required=True)
    last_name = forms.CharField(widget=forms.TextInput(), label='Last Name', min_length='2', required=True)
    relationship = forms.ModelChoiceField(queryset=NextOfKinRelation.objects.all(), label="Relationship",
                                          required=True)
    phone_number = PhoneNumberField(help_text="Must start with +254", )
    email = forms.EmailField(label='Kin email', help_text="first_namelast_name@domain.com", required=True)

    class Meta:
        model = WorkerNextOfKin
        fields = ('first_name', 'last_name', 'relationship', 'phone_number', 'email')


class IncomingJobForm(forms.ModelForm):

    service_category = forms.Select()
    service_name = ChainedSelect(
        app_name='goodlyf',
        model_name='IncomingJob',
        chain_field='category_name',
        model_field='service_category',
        auto_choose=True,
        show_all=False
    )

    date_job_starts = forms.DateTimeField(widget=forms.DateInput, label='Pick day', required=True, )
    time_remaining = forms.CharField(required=False)

    class Meta:
        model = IncomingJob
        fields = ('service_category', 'service_name', 'date_job_starts')
        exclude = ('date_posted', )


class JobAssignmentForm(forms.ModelForm):
    class MyModelChoiceField(forms.ModelChoiceField):
        def clean(self, value):
            return value

        def label_from_instance(self, obj):
            return obj.id

    job = MyModelChoiceField(queryset=IncomingJob.objects.all(), widget=forms.Select())
    assigned_to = forms.ModelChoiceField(
        queryset=Worker.objects.all().filter(worker_category__category_name='Supervisor'))
    date_job_ends = forms.DateField(widget=forms.DateInput, label='Ends On', required=True)
    state = forms.ChoiceField(choices=JobAssignment.STATES, label='Status', widget=forms.Select(), required=True)
    push_state = forms.ChoiceField(choices=JobAssignment.PUSH_STATES, label='push', widget=forms.Select(), required=True)

    class Meta:
        model = JobAssignment
        fields = ('assigned_to', 'date_job_ends', 'state')



class JobFeedbackForm(forms.ModelForm):
    class MyModelChoiceField(forms.ModelChoiceField):
        def label_from_instance(self, obj):
            return obj.id

        def clean(self, value):
            return value

    job = MyModelChoiceField(queryset=JobAssignment.objects.all())

    comment = forms.CharField(label='Comment', widget=forms.Textarea(), required=True)

    class Meta:
        model = JobFeedback
        fields = ('comment', )


class ClientForm(forms.ModelForm):
    class MyModelChoiceField(forms.ModelChoiceField):
        def label_from_instance(self, obj):
            return obj.id

    job = MyModelChoiceField(queryset=IncomingJob.objects.all())
    name = forms.CharField(label='Name', widget=forms.TextInput(), min_length=2, max_length=20,
                           error_messages={'required': 'Please type your name'})
    phone_number = PhoneNumberField(unique=False, error_messages={'required': 'Please type your phone number',
                                                                  'invalid': 'Please enter a valid phone number'})
    email = forms.EmailField(widget=forms.EmailInput(), error_messages={'required': 'Please type your email'})
    area = forms.CharField(label='Area', widget=forms.TextInput(), min_length=2, max_length=20,
                           error_messages={'required': 'Please type your area'})
    street = forms.CharField(label='Street', widget=forms.TextInput(), min_length=2, max_length=20,
                             error_messages={'required': 'Please type your street'})
    estate = forms.CharField(label='Estate', widget=forms.TextInput(), min_length=2, max_length=20,
                             error_messages={'required': 'Please type your estate'})
    house_number = forms.IntegerField(required=False)

    class Meta:
        model = Client
        fields = ('name', 'phone_number', 'email', 'area', 'street', 'estate', 'house_number')
        exclude = ('job', )
