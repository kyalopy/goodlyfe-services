from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import User
from datetime import datetime
from django.core.validators import RegexValidator
from smart_selects.db_fields import ChainedForeignKey
from extended_choices import Choices



class WorkerCategory(models.Model):
    category_name = models.CharField(max_length=40, unique=True, blank=False)

    def __str__(self):
        return self.category_name


class Worker(models.Model):

    user = models.OneToOneField(User,)
    worker_category = models.ForeignKey(WorkerCategory,)
    phone_number = PhoneNumberField(unique=True)
    national_id = models.PositiveIntegerField(max_length=10, unique=True, blank=False)
    year_of_birth = models.DateField(unique=False, default='1975-01-01')
    profile_image = models.ImageField(verbose_name='Avatar', )

    def __str__(self):
        return self.user.username


class NextOfKinRelation(models.Model):
    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    name = models.CharField(max_length=50, blank=False, validators=[alpha_validator])

    def __str__(self):
        return self.name


class WorkerNextOfKin(models.Model):
    worker = models.OneToOneField(Worker, unique=True)

    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')

    first_name = models.CharField(max_length=40, blank=False, validators=[alpha_validator])
    last_name = models.CharField(max_length=40, blank=False, validators=[alpha_validator])
    relationship = models.ForeignKey(NextOfKinRelation)
    phone_number = PhoneNumberField(unique=True)
    email = models.EmailField(unique=True, blank=True)


class WorkerResidence(models.Model):
    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    country = models.CharField(max_length=25, blank=False, default='Kenya', validators=[alpha_validator])
    county = models.CharField(max_length=25,  blank=False, validators=[alpha_validator])
    sub_county = models.CharField(max_length=25,  blank=False, validators=[alpha_validator])
    location = models.CharField(max_length=25, blank=False, verbose_name='Street', validators=[alpha_validator])
    village = models.CharField(max_length=25, blank=False, verbose_name='Physical location', validators=[alpha_validator])
    worker = models.OneToOneField(Worker, unique=True)

    def __str__(self):
        return self.county


class Skill(models.Model):
    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    name = models.CharField(max_length=100, blank=False, unique=True, validators=[alpha_validator])

    def __str__(self):
        return self.name


class WorkerSkill(models.Model):
    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    worker = models.OneToOneField(Worker, unique=True)
    skill = models.ForeignKey(Skill)
    extra_skill = models.CharField(max_length=30, unique=False, blank=True, validators=[alpha_validator])


class ServiceCategory(models.Model):

    category_name = models.CharField(max_length=40, unique=True, blank=False, verbose_name='Services')

    def __str__(self):
        return self.category_name


class ServiceDuration(models.Model):
    """ half a day or full day """
    duration = models.CharField(max_length=50, blank=False, unique=True, default='Half Day', )

    def __str__(self):
        return self.duration


class ServicePricing(models.Model):
    price = models.CharField(max_length=5, unique=True, blank=False, default='1000')

    def __str__(self):
        return self.price


class Service(models.Model):
    service_category = models.ForeignKey(ServiceCategory)
    name = models.CharField(max_length=200, blank=False, )

    def __str__(self):
        return self.name


class IncomingJob(models.Model):
    STATES = Choices(
        ('NOT_SEEN', 0, 'Not Seen'),
        ('SEEN', 1, 'Seen')
    )
    ASSIGNMENT = Choices(
        ('NOT_ASSIGNED', 0, 'Not Assigned'),
        ('Assigned', 1, 'Assigned')
    )
    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    service_category = models.ForeignKey(ServiceCategory)
    service_name = ChainedForeignKey(
        Service,
        chained_field="service_category",
        chained_model_field="service_category",
        auto_choose=True,
        show_all=False
    )

    date_posted = models.DateTimeField(auto_now_add=True, verbose_name='Received On')
    date_job_starts = models.DateField(verbose_name='Starts On', default=datetime.now)
    state = models.SmallIntegerField(choices=STATES, default=STATES.NOT_SEEN)
    assignment = models.SmallIntegerField(choices=ASSIGNMENT, default=ASSIGNMENT.NOT_ASSIGNED)
    time_remaining = models.PositiveIntegerField(default=0, blank=True)

    class Meta:
        ordering = ['-date_posted']

    def __str__(self):
        return self.service_category.category_name


class JobAssignment(models.Model):
    PUSH_STATES = Choices(
        ('NOT_PUSHED', 0, 'Not pushed'),
        ('PUSHED', 1, 'Pushed')
    )
    STATES = Choices(
        ('PENDING', 0, 'Pending'),
        ('IN PROGRESS', 1, 'In Progress'),
        ('COMPLETE', 2, 'complete'),
        ('CANCELLED', 3, 'Cancelled'),
    )

    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    job = models.ForeignKey(IncomingJob)
    assigned_to = models.ForeignKey(Worker)
    date_job_ends = models.DateField(verbose_name='Ends On')
    state = models.SmallIntegerField(choices=STATES, default=STATES.PENDING)
    push_state = models.SmallIntegerField(choices=PUSH_STATES, default=PUSH_STATES.NOT_PUSHED)

    def __str__(self):
        return self.job.service_category.category_name

    class Meta:
        ordering = ['-assigned_to']


class FeedbackCategory(models.Model):
    category_name = models.CharField(max_length=50, blank=False, unique=True, )

    def __str__(self):
        return self.category_name


class ServiceMenu(models.Model):
    category = models.ForeignKey(ServiceCategory)
    name = models.OneToOneField(Service)
    time = models.ForeignKey(ServiceDuration)
    price = models.ForeignKey(ServicePricing)

    def __str__(self):
        return self.name.name


class SupervisorAssignment(models.Model):
    supervisor = models.ForeignKey(Worker)
    handyman = models.OneToOneField(User,)

    def __str__(self):
        return self.handyman.username


class JobFeedback(models.Model):

    job = models.ForeignKey(JobAssignment)
    comment = models.CharField(max_length=2500, blank=True)
    date_posted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.job.job.service_name


class Client(models.Model):
    job = models.ForeignKey(IncomingJob)
    name = models.CharField(max_length=30, unique=False, blank=False, )
    email = models.EmailField(blank=True)
    phone_number = PhoneNumberField(unique=False)
    area = models.CharField(max_length=30, unique=False, blank=False)
    street = models.CharField(max_length=30, unique=False, blank=False)
    estate = models.CharField(max_length=30, unique=False, blank=False)
    house_number = models.IntegerField(blank=True, default=None, null=True)

    def __str__(self):
        return self.name


class Feedback(models.Model):
    alpha_validator = RegexValidator(r'^[a-zA-Z]', 'Only alphabetic characters are allowed.')
    reviewed_job = models.ForeignKey(IncomingJob)
    client = models.ForeignKey(Client)
    title = models.ForeignKey(FeedbackCategory)
    text = models.CharField(max_length=1000, blank=False, validators=[alpha_validator])